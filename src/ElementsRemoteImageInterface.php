<?php

namespace Drupal\elements_db_client;

/**
 * Service to get publication images.
 */
interface ElementsRemoteImageInterface {

  /**
   * Get image for a publication.
   *
   * @param int $publication_id
   *   ID of publication in Symplectic Elements.
   *
   * @return array
   *   Render array for the image
   */
  public function getImage(int $publication_id): array;

}
