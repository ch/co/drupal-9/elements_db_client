<?php

namespace Drupal\elements_db_client\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Formatter to display the image for a publication.
 *
 * @ViewsField("symplectic_image")
 */
class SymplecticImageFormatter extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $value = $this->getValue($values);
    return \Drupal::service('elements_db_client.elements_remote_image')->getImage($value);
  }

}
