<?php

namespace Drupal\elements_db_client;

use Drupal\Core\Cache\CacheBackendInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Get publication image from a remote server.
 */
class ElementsRemoteImage implements ElementsRemoteImageInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    private readonly ClientInterface $httpClient,
    private readonly CacheBackendInterface $elementsImageCache,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function getImage(int $publication_id): array {
    $image_path = "https://publications.ch.cam.ac.uk/publication-image/$publication_id";

    $cid = 'elements_remote_image:' . $publication_id;

    if ($cache = $this->elementsImageCache->get($cid)) {
      $image = $cache->data;
    }
    else {

      $image = [];

      try {
        $response = $this->httpClient->request('GET', $image_path);
        if ($response->getStatusCode() == 200) {
          $image = [
            '#type' => 'container',
            '#attributes' => ['class' => 'publication-image'],
            'image' => ['#markup' => "<img src='$image_path'/>"],
          ];
        }

      }
      catch (GuzzleException $e) {
      }

      $tags = [$cid];

      $this->elementsImageCache->set($cid, $image, strtotime('+24 hours'), $tags);
    }
    return $image;
  }

}
